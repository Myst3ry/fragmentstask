package com.myst3ry.fragmentstask;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public final class DocumentFragment extends Fragment {

    public static final String TAG = DocumentFragment.class.getSimpleName();
    private static final String ARG_DOCUMENT_NUMBER = BuildConfig.APPLICATION_ID + "arg.document_number";

    private int number;

    public static DocumentFragment newInstance(final int docNumber) {
        final DocumentFragment fragment = new DocumentFragment();
        final Bundle args = new Bundle();
        args.putInt(ARG_DOCUMENT_NUMBER, docNumber);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            number = getArguments().getInt(ARG_DOCUMENT_NUMBER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_document, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView docNumberTextView = view.findViewById(R.id.tv_document_number);
        docNumberTextView.setText(getString(R.string.document_number, number));
    }
}
