package com.myst3ry.fragmentstask;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public final class MainActivity extends AppCompatActivity {

    private TextView emptyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        emptyTextView = findViewById(R.id.tv_documents_empty);

        initButtons();
        checkDocumentsCount();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        deleteAllDocuments();
    }

    private void initButtons() {
        final Button btnAddDocument = findViewById(R.id.btn_document_add);
        final Button btnDeleteDocument = findViewById(R.id.btn_document_delete);

        btnAddDocument.setOnClickListener(v -> addDocument());
        btnDeleteDocument.setOnClickListener(v -> deleteDocument());
    }

    private void addDocument() {
        emptyTextView.setVisibility(View.GONE);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.documents_container,
                        DocumentFragment.newInstance(getSupportFragmentManager().getBackStackEntryCount() + 1),
                        DocumentFragment.TAG)
                .addToBackStack(DocumentFragment.TAG)
                .commit();
    }

    private void deleteDocument() {
        getSupportFragmentManager().popBackStackImmediate();
        checkDocumentsCount();
    }

    private void deleteAllDocuments() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        checkDocumentsCount();
    }

    private void checkDocumentsCount() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            emptyTextView.setVisibility(View.VISIBLE);
        } else {
            emptyTextView.setVisibility(View.GONE);
        }
    }
}
